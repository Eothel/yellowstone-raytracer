package be.rivendale.ghetto;

import be.rivendale.material.Color;
import be.rivendale.mathematics.Triple;
import be.rivendale.mathematics.Vector;
import org.junit.Test;

import static be.rivendale.mathematics.MathematicalAssert.assertColorEquals;
import static be.rivendale.mathematics.MathematicalAssert.assertVectorEquals;

public class VoxelTest {
	@Test
	public void colorCanBeSetAndRetrieved() {
		Voxel voxel = new Voxel(Color.GREEN, new Triple(0, 0, 1));
		assertColorEquals(Color.GREEN, voxel.getColor());
	}

	@Test
	public void normalCanBeSetAndRetrieved() {
		Voxel voxel = new Voxel(Color.GREEN, new Triple(0, 0, 1));
		assertVectorEquals(new Triple(0, 0, 1), voxel.getNormal());
	}

	@Test(expected = IllegalArgumentException.class)
	public void colorMustNotBeNull() {
		new Voxel(null, new Triple(0, 0, 1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void normalMustNotBeNull() {
		new Voxel(Color.BLUE, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void normalMustBeNormalized() {
	    new Voxel(Color.RED, new Triple(0, 0, 0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void normalMustNotBeZeroVector() {
	    new Voxel(Color.RED, Vector.ZERO_VECTOR);
	}
}
