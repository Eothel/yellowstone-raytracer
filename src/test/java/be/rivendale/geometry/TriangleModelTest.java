package be.rivendale.geometry;

import be.rivendale.mathematics.MathematicalAssert;
import be.rivendale.mathematics.Triangle;
import be.rivendale.mathematics.Triple;
import be.rivendale.mathematics.Vertex;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static be.rivendale.mathematics.MathematicalAssert.assertPointEquals;

public class TriangleModelTest {
    private static final String TEST_DATA_DIRECTORY_PREFIX = "./src/test/data/";

    @Test
	public void constructorForListOfTrianglesInitializesModelCorrectly() throws Exception {
		TriangleModel triangleModel = new TriangleModel(Arrays.asList(
				new Triangle(new Vertex(1, 5, 6), new Vertex(3, -2, 8), new Vertex(6, 4, 7)),
				new Triangle(new Vertex(1, 2, 3), new Vertex(8, 7, 6), new Vertex(5, 6, 7))
		));
		List<Triangle> list = triangleModel.getTriangles();
		MathematicalAssert.assertTriangleEquals(new Triangle(
			new Vertex(1, 5, 6),
			new Vertex(3, -2, 8),
			new Vertex(6, 4, 7)
		), list.get(0));
		MathematicalAssert.assertTriangleEquals(new Triangle(
			new Vertex(1, 2, 3),
			new Vertex(8, 7, 6),
			new Vertex(5, 6, 7)
		), list.get(1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorForListOfTrianglesRequiresListNotToBeNull() {
	    new TriangleModel(null);
	}

    @Test
	public void axisAlignedBoundingBoxCanBeRetrievedAroundTriangleModel() {
	    TriangleModel model = RawFileLoader.loadRawFile(TEST_DATA_DIRECTORY_PREFIX + "validRawFile.txt", new Triple(0, 0, 0));
                AxisAlignedBoundingBox axisAlignedBoundingBox = model.getAxisAlignedBoundingBox();
		assertPointEquals(new Triple(-0.707106, 0.000000, -0.707107), axisAlignedBoundingBox.getMinimumBound());
		assertPointEquals(new Triple(1.707107, 1.414214, 1.707107), axisAlignedBoundingBox.getMaximumBound());
	}
}
