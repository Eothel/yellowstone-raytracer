package be.rivendale.geometry;

import be.rivendale.mathematics.LineNovo;
import be.rivendale.mathematics.Rectangle;
import be.rivendale.mathematics.Triple;
import org.junit.Test;

import static be.rivendale.mathematics.MathematicalAssert.*;

public class CameraTest {

    public static final int DEFAULT_X_AND_Y_RESOLUTION = 10;
    public static final Camera DEFAULT_CAMERA = new Camera();

    @Test
    public void constructorSetsFocusPointCorrectly() throws Exception {
        assertPointEquals(new Triple(0, 0, -5), new Camera().getFocusPoint());
    }

    @Test
    public void constructorSetsViewPortCorrectly() throws Exception {
        assertRectangleEquals(new Rectangle(new Triple(-4, -3, 0),
                new Triple(-4, 3, 0),
                new Triple(4, -3, 0)
        ), new Camera().getViewPlane());
    }

    @Test
    public void castRayReturnsRayThroughCenterOfTopLeftCornerPixelWhenBothPixelIndicesAreZero() throws Exception {
        LineNovo ray = DEFAULT_CAMERA.castRay(0, 0, DEFAULT_X_AND_Y_RESOLUTION, DEFAULT_X_AND_Y_RESOLUTION);
        Triple corner = (Triple) DEFAULT_CAMERA.getTopLeftCorner();
        Triple expectedPoint = corner.add(centerOfPixelOffset(DEFAULT_X_AND_Y_RESOLUTION, false, true));
        assertRayEquals(new LineNovo(DEFAULT_CAMERA.getFocusPoint(), expectedPoint), ray);
    }

    @Test
    public void castRayReturnsRayThroughCenterOfTopRightCornerPixelWhenTheHorizontalPixelIndexIsAtMaximumAndTheVerticalPixelIndexIsZero() throws Exception {
        LineNovo ray = DEFAULT_CAMERA.castRay(DEFAULT_X_AND_Y_RESOLUTION - 1, 0, DEFAULT_X_AND_Y_RESOLUTION, DEFAULT_X_AND_Y_RESOLUTION);
        Triple corner = (Triple) DEFAULT_CAMERA.getTopRightCorner();
        Triple expectedPoint = corner.add(centerOfPixelOffset(DEFAULT_X_AND_Y_RESOLUTION, true, true));
        assertRayEquals(new LineNovo(DEFAULT_CAMERA.getFocusPoint(), expectedPoint), ray);

    }

    @Test
    public void castRayReturnsRayThroughCenterOfBottomLeftCornerPixelWhenTheVerticalPixelIndexIsAtMaximumAndTheHorizontalPixelIndexIsZero() throws Exception {
        LineNovo ray = DEFAULT_CAMERA.castRay(0, DEFAULT_X_AND_Y_RESOLUTION - 1, DEFAULT_X_AND_Y_RESOLUTION, DEFAULT_X_AND_Y_RESOLUTION);
        Triple corner = (Triple) DEFAULT_CAMERA.getBottomLeftCorner();
        Triple expectedPoint = corner.add(centerOfPixelOffset(DEFAULT_X_AND_Y_RESOLUTION, false, false));
        assertRayEquals(new LineNovo(DEFAULT_CAMERA.getFocusPoint(), expectedPoint), ray);
    }

    @Test
    public void castRayReturnsRayThroughCenterOfBottomRightCornerPixelWhenBothPixelIndicesAreAtMaximum() throws Exception {
        LineNovo ray = DEFAULT_CAMERA.castRay(DEFAULT_X_AND_Y_RESOLUTION - 1, DEFAULT_X_AND_Y_RESOLUTION - 1, DEFAULT_X_AND_Y_RESOLUTION, DEFAULT_X_AND_Y_RESOLUTION);
        Triple corner = (Triple) DEFAULT_CAMERA.getBottomRightCorner();
        Triple expectedPoint = corner.add(centerOfPixelOffset(DEFAULT_X_AND_Y_RESOLUTION, true, false));
        assertRayEquals(new LineNovo(DEFAULT_CAMERA.getFocusPoint(), expectedPoint), ray);
    }

    /**
     * The center of the pixel has an offset of half the width or height of one pixel.
     */
    private Triple centerOfPixelOffset(int xAndYResolution, boolean insetX, boolean insetY) {
        int insetXMultiplier = insetX ? -1 : 1;
        int insetYMultiplier = insetY ? -1 : 1;
        return new Triple(
            insetXMultiplier * DEFAULT_CAMERA.horizontalViewPlaneSize() / xAndYResolution / 2,
            insetYMultiplier * DEFAULT_CAMERA.verticalViewPlaneSize() / xAndYResolution / 2,
            0
        );
    }

    @Test
    public void getTopLeftCornerReturnsTopPointBOfViewPort() throws Exception {
        Camera camera = new Camera();
        assertPointEquals(camera.getViewPlane().getB(), camera.getTopLeftCorner());
    }

    @Test
    public void getTopRightCornerReturnsPointDOfViewPort() throws Exception {
        Camera camera = new Camera();
        assertPointEquals(camera.getViewPlane().getD(), camera.getTopRightCorner());
    }

    @Test
    public void getBottomLeftCornerReturnsPointAOfViewPort() throws Exception {
        Camera camera = new Camera();
        assertPointEquals(camera.getViewPlane().getA(), camera.getBottomLeftCorner());
    }

    @Test
    public void getBottomRightCornerReturnsPointCOfViewPort() throws Exception {
        Camera camera = new Camera();
        assertPointEquals(camera.getViewPlane().getC(), camera.getBottomRightCorner());
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenXIsNegative() {
        new Camera().castRay(-1, 10, 20, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenYIsNegative() {
        new Camera().castRay(10, -1, 20, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenXIsGreaterThenWith() {
        new Camera().castRay(21, 10, 20, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenXEqualToWith() {
        new Camera().castRay(20, 10, 20, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenYIsGreaterThenHeight() {
        new Camera().castRay(10, 21, 20, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenYEqualToHeight() {
        new Camera().castRay(10, 20, 20, 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenWidthIsLessThanOne() {
        new Camera().castRay(0, 0, 0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void castRayThrowsIllegalArgumentExceptionWhenHeightIsLessThanOne() {
        new Camera().castRay(0, 0, 1, 0);
    }

	@Test
	public void horizontalViewAngleReturnsCorrectAngleOfView() throws Exception {
		assertDoubleEquals(1.349482, new Camera().horizontalViewAngle());
	}

	@Test
	public void verticalViewAngleReturnsCorrectAngleOfView() throws Exception {
		assertDoubleEquals(1.080839, new Camera().verticalViewAngle());
	}

	@Test
	public void diagonalViewAngleReturnsCorrectAngleOfView() throws Exception {
		assertDoubleEquals(1.570796, new Camera().diagonalViewAngle());
	}

	@Test
	public void focalLengthReturnsDistanceFromFocusPointToCenterOfViewPlane() throws Exception {
		assertDoubleEquals(5, new Camera().focalLength());
	}

	@Test
	public void horizontalViewPlaneSizeCalculatesTheHorizontalSizeOfTheFilm() throws Exception {
		assertDoubleEquals(8, new Camera().horizontalViewPlaneSize());
	}

	@Test
	public void verticalViewPlaneSizeCalculatesTheVerticalSizeOfTheFilm() throws Exception {
		assertDoubleEquals(6, new Camera().verticalViewPlaneSize());
	}

	@Test
	public void diagonalViewPlaneSizeCalculatesTheDiagonalSizeOfTheFilm() throws Exception {
		assertDoubleEquals(10, new Camera().diagonalViewPlaneSize());
	}
}
