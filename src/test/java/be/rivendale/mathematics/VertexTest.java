package be.rivendale.mathematics;

import be.rivendale.material.Color;
import org.junit.Test;

import static be.rivendale.mathematics.MathematicalAssert.assertColorEquals;
import static be.rivendale.mathematics.MathematicalAssert.assertDoubleEquals;

public class VertexTest {
    @Test
    public void constructorSetsXCoordinate() throws Exception {
        Vertex vertex = new Vertex(1, 2, 3, Color.RED);
        assertDoubleEquals(1, vertex.getX());
    }

    @Test
    public void constructorSetsYCoordinate() throws Exception {
        Vertex vertex = new Vertex(1, 2, 3, Color.RED);
        assertDoubleEquals(2, vertex.getY());
    }

    @Test
    public void constructorSetsZCoordinate() throws Exception {
        Vertex vertex = new Vertex(1, 2, 3, Color.RED);
        assertDoubleEquals(3, vertex.getZ());
    }

    @Test
    public void constructorSetsColor() throws Exception {
        Vertex vertex = new Vertex(1, 2, 3, Color.RED);
        assertColorEquals(Color.RED, vertex.getColor());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsIllegalArgumentExceptionWhenColorIsNull() throws Exception {
        new Vertex(1, 2, 3, null);
    }

    @Test
    public void constructorWithoutColorSetsColorToWhite() throws Exception {
        Vertex vertex = new Vertex(1, 2, 3);
        assertColorEquals(Color.WHITE, vertex.getColor());
    }
}
