package be.rivendale.mathematics.intersection;

import org.junit.Test;

import static be.rivendale.mathematics.intersection.PolygonIntersectionMode.parallelogram;
import static be.rivendale.mathematics.intersection.PolygonIntersectionMode.triangle;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PolygonIntersectionModeTest {
	@Test
	public void trianglePolygonIntersectionModeReturnsTrueIfSumOfUAndVIsLessThenOne() throws Exception {
		assertTrue(triangle.isValidIntersectionPoint(0.4, 0.5));
	}

	@Test
	public void trianglePolygonIntersectionModeReturnsTrueIfSumOfUAndVEqualsOne() throws Exception {
		assertTrue(triangle.isValidIntersectionPoint(0.3, 0.7));
	}

	@Test
	public void trianglePolygonIntersectionModeReturnsFalseIfSumOfUAndVIsLargerThenOne() throws Exception {
		assertFalse(triangle.isValidIntersectionPoint(0.2, 0.9));
	}

	@Test
	public void parallelogramPolygonIntersectionModeReturnsTrueIfSumOfUAndVEqualsOne() throws Exception {
		assertTrue(parallelogram.isValidIntersectionPoint(0.3, 0.7));
	}

	@Test
	public void parallelogramPolygonIntersectionModeReturnsTrueIfSumOfUAndVIsLessThenOne() throws Exception {
		assertTrue(parallelogram.isValidIntersectionPoint(0.3, 0.2));
	}

	@Test
	public void parallelogramPolygonIntersectionModeReturnsTrueIfSumOfUAndVIsLargerThenOne() throws Exception {
		assertTrue(parallelogram.isValidIntersectionPoint(0.8, 0.7));
	}
}
