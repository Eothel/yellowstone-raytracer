package be.rivendale.renderer;

import be.rivendale.geometry.AxisAlignedBoundingBox;
import be.rivendale.geometry.Camera;
import be.rivendale.geometry.TriangleModel;
import be.rivendale.ghetto.RayBoxIntersectionGhetto;
import be.rivendale.ghetto.Shader;
import be.rivendale.material.Color;
import be.rivendale.mathematics.*;
import be.rivendale.mathematics.intersection.Intersection;
import be.rivendale.mathematics.intersection.RayIntersectionMode;
import be.rivendale.renderer.novo.RenderAction;
import be.rivendale.renderer.novo.Scene;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @deprecated To be replaced by RenderAction
 */
@Deprecated
public class RenderTask implements Callable<ScreenPart> {
    private OverlayRenderer overlayRenderer = new OverlayRenderer();

    private int x;
    private int y;
    private int width;
    private int height;
    private int screenWidth;
    private int screenHeight;
    private final Scene scene;
    private static double thickness = 0.1;

    private AxisAlignedBoundingBox lightBeacon;

    public RenderTask(int x, int y, int width, int height, int screenWidth, int screenHeight, Scene scene) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.scene = scene;
        lightBeacon = new AxisAlignedBoundingBox(
                new Triple(scene.light().getX() - thickness / 2, scene.light().getY() - thickness / 2, scene.light().getZ() - thickness / 2),
                new Triple(scene.light().getX() + thickness / 2, scene.light().getY() + thickness / 2, scene.light().getZ() + thickness / 2)
        );
    }

    public ScreenPart call() throws Exception {
        ScreenPart screenPart = new ScreenPart(x, y, renderScreenPartAsImage());
        overlayRenderer.drawOverlay(screenPart);
        return screenPart;
	}

	private BufferedImage renderScreenPartAsImage() {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		traceAllRays(image);
		return image;
	}

    private void traceAllRays(BufferedImage image) {
		for(int yCoordinate = 0; yCoordinate < height; yCoordinate++) {
			traceScanLine(image, yCoordinate);
		}
	}

	private void traceScanLine(BufferedImage image, int yCoordinate) {
		for(int xCoordinate = 0; xCoordinate < width; xCoordinate++) {
			Color color = traceRay(yCoordinate, xCoordinate);
			image.getRaster().setPixel(xCoordinate, yCoordinate, color.toArray());
		}
	}

	private Color traceRay(int yCoordinate, int xCoordinate) {
        LineNovo ray = scene.camera().castRay(xCoordinate + x, yCoordinate + y, screenWidth, screenHeight);
        Ray oldray = new Ray(ray.a(), ray.b());
//        Color color = voxelGrid.traverseVoxels(oldray);
//        if(color != null) {
//            return color;
//        }

        Double tl = RayBoxIntersectionGhetto.rayBoxIntersection(oldray, lightBeacon);
        if(tl != null && tl >= 1) {
            return Color.RED;
        }

        Color color = Color.BLACK;
        if(scene.model().getAxisAlignedBoundingBox() != null && RayBoxIntersectionGhetto.rayBoxIntersection(oldray, scene.model().getAxisAlignedBoundingBox()) != null) {
            color = color.add(Color.RED.multiply(0.5));

            double closestT = Double.MAX_VALUE;
            Triangle closestTriangle = null;

            for(Triangle triangle : scene.model().getTriangles()) {
                if(triangle.normal().dotProduct(ray.direction()) >= 0) { // Is it not back-facing? - can be eliminated by implementing scratchapixel's muller trumbore
                    // Backfacing for this ray (not necessarily for all rays of this frame if the camera has perspective.
                } else {
                    double t = ray.intersect(triangle);
                    if(t >= 1 && t < closestT) { // Is this a hit and closer to any previous hit?
                        closestTriangle = triangle;
                        closestT = t;
                    }
                }
            }
            if(closestTriangle != null) {
                color = calculateSampleColor(ray.solve(closestT), closestTriangle);

                Ray projectionRay = new Ray(scene.camera().getFocusPoint(), closestTriangle.getA());
                Intersection intersection = scene.camera().getViewPlane().intersection(projectionRay, RayIntersectionMode.betweenOriginAndPassThroughPoint);
//                System.out.println(intersection.getT() + " " + intersection.getU() + " " + intersection.getV());

                double xSize = scene.camera().verticalViewPlaneSize() / screenHeight;
                double ySize = scene.camera().horizontalViewPlaneSize() / screenWidth;
                System.out.println(xSize);
                System.out.println(ySize);

                int x = (int)(scene.camera().horizontalViewPlaneSize() * intersection.getV() / xSize);
                int y = (int)(scene.camera().verticalViewPlaneSize() * intersection.getU() / ySize);
                System.out.println(x + " " + y);
            }
        }

        return color;
    }

    private Color calculateSampleColor(Point intersectionPoint, Triangle triangle) {
        return Shader.shade(scene.light(), triangle.normal(), ((Vertex) triangle.getA()).getColor(), intersectionPoint);
    }

    public List<RenderTask> split() {
        return splitUntilSmallerThen(Arrays.asList(this), 100);
    }

    private List<RenderTask> splitIntoTwo() {
        ArrayList<RenderTask> rectangles = new ArrayList<RenderTask>();
        if(width >= height) {
            int newLeftWidth = width / 2;
            int newRightWidth = width - newLeftWidth;
            rectangles.add(new RenderTask(x, y, newLeftWidth, height, screenWidth, screenHeight, scene));
            rectangles.add(new RenderTask(x + newLeftWidth, y, newRightWidth, height, screenWidth, screenHeight, scene));
        } else {
            int newTopHeight = height / 2;
            int newBottomHeight = height - newTopHeight;
            rectangles.add(new RenderTask(x, y, width, newTopHeight, screenWidth, screenHeight, scene));
            rectangles.add(new RenderTask(x, y + newTopHeight, width, newBottomHeight, screenWidth, screenHeight, scene));
        }
        return rectangles;
    }

    private List<RenderTask> splitUntilSmallerThen(List<RenderTask> tasks, int maximumTaskDimension) {
        RenderTask anyTask = tasks.get(0);
        if(anyTask.width > maximumTaskDimension || anyTask.height > maximumTaskDimension) {
            return splitUntilSmallerThen(splitAll(tasks), maximumTaskDimension);
        } else {
            return tasks;
        }
    }

    private List<RenderTask> splitAll(List<RenderTask> tasks) {
        ArrayList<RenderTask> splitTasks = new ArrayList<RenderTask>();
        for (RenderTask task : tasks) {
            splitTasks.addAll(task.splitIntoTwo());
        }
        return splitTasks;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}

