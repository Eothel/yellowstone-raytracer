package be.rivendale.renderer;

import org.apache.commons.lang.Validate;

import java.awt.image.BufferedImage;

/**
 * Represents a part of the screen used to render the results of the raytracing to.
 * A screen part consists of a {@link BufferedImage} which defines it's pixel raster content, and
 * a X and Y location, which define the top left corner on the screen of which this is a part.
 * <p>Since a buffered image has a width and height, this screenpart marks a rectangular region on the actual screen.</p>
 * <p>A screen part is interesting in order to do parallel computation of multiple regions on the screen at the same time.</p>
 * @deprecated To be replaced with rendering on the Graphics of the canvas's backing buffer.
 */
@Deprecated
public class ScreenPart {
    /**
     * The x position marking the relative position of this screen part on it's parent screen.
     */
    private int x;

    /**
     * The y position marking the relative position of this screen part on it's parent screen.
     */
    private int y;

    /**
     * The image containing the actual pixel raster and it's width and height of this screen part.
     */
    private BufferedImage image;

    /**
     * Creates a new screen part, on the specified relative position and with the specified image pixel raster.
     * @param x The relative position of this screen part on the parent screen.
     * @param y The relative y position of this screen part on the parent screen.
     * @param image The image conaining the pixel data of this screen rectangle.
     */
    public ScreenPart(int x, int y, BufferedImage image) {
        Validate.notNull(image);
        Validate.isTrue(x >= 0 && y >= 0);
        this.x = x;
        this.y = y;
        this.image = image;
    }

    /**
     * Retrieves the relative x position of this screen part.
     * @return The horizontal relative position on a 2D raster.
     */
    public int getX() {
        return x;
    }

    /**
     * Retrieves the relative y position of this screen part.
     * @return The vertical relative position on a 2D raster.
     */
    public int getY() {
        return y;
    }

    /**
     * Retrieves the screen part's actual pixel data as a buffered image.
     * @return The image containing the screen part's pixel data.
     */
    public BufferedImage getImage() {
        return image;
    }
}
