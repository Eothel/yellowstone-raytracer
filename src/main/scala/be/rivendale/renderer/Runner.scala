package be.rivendale.renderer

import be.rivendale.geometry.{Camera, RawFileLoader}
import be.rivendale.mathematics.{Vertex, Triple}
import be.rivendale.renderer.novo.Scene

object Runner {
  def main(arguments: Array[String]) {
    println("Launching the RayTracer from Scala")
//    val triangleModel = RawFileLoader.loadRawFile("./src/main/data/torus.raw.txt", new Triple(0, 0, 2))
//    val triangleModel = RawFileLoader.loadRawFile("./src/main/data/sphere.raw.txt", new Triple(0, 0, 1))
    val triangleModel = RawFileLoader.loadRawFile("./src/main/data/triangle.raw.txt", new Triple(0, 0, 2))
    new Renderer(1920 / 3, 1080 / 3, 4, new Scene(new Vertex(0, 0, -5), triangleModel, new Camera())).start()
  }
}