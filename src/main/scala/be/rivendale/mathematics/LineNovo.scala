package be.rivendale.mathematics

import be.rivendale.mathematics.intersection.{RayIntersectionMode, Intersection}

class LineNovo(val a: Point, val b: Point) {
  val direction = (b - a).asInstanceOf[Vector]

  def solve(t: Double) = a + (direction * t).asInstanceOf[Point]

  def intersect(triangle : Triangle): Double = {
    val ray: Ray = new Ray(a, b)
    val intersection: Intersection = new Intersection(ray, triangle, RayIntersectionMode.anywhere)
    intersection.getT()
  }
}
