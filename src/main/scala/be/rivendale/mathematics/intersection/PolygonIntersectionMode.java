package be.rivendale.mathematics.intersection;

/**
 * Determines the intersection mode regarding to the polygon on a {@link be.rivendale.mathematics.intersection.Intersection ray polygon intersection test}.
 * <p>The algorythm used for the ray-triangle intersection test can be easily extended to include all sorts of
 * parallelograms as well (including rectangles). This is merely done by allowing the sum of the u and v coordinates
 * (which is one of the results of the ray-triangle intersection test) to be <code>&gt; 1</code>. With triangles, this
 * is constrained to <code>&lt;= 1.</code></p>
 */
public enum PolygonIntersectionMode {
	/**
	 * Determines the intersection mode for a triangle. This means that the {@link be.rivendale.mathematics.intersection.Intersection intersection algorithm}
	 * will be configured to regard the parametric values u and v as being the barycentric coordinates of the triangle.
	 * This implies that the third barycentric coordinate can be calculated as <code>w = 1 - u - v</code>, and that the
	 * value of w is never larger then one (this is the test condition to check if an intersection is within the bounds
	 * of the triangle).
	 */
	triangle {
		public boolean isValidIntersectionPoint(double u, double v) {
			return u + v <= 1;
		}
	},

	/**
	 * Determines the intersection mode for a parallellogram (including rectangles, since a rectangle is a type of
	 * parallellogram). This means that the {@link be.rivendale.mathematics.intersection.Intersection intersection algorithm} will be configured to regard the
	 * parametric values u and v as being the relative psoition (ranging from 0..1) inside the parallellogram's bounds.
	 * <p>The only difference with the {@link #triangle} mode is that there is no constraint regarding to the sum of u
	 * and v other then that both u and v must range from 0..1 (and thus the sum of the two ranges from 0..2)</p>
	 */
	parallelogram {
		public boolean isValidIntersectionPoint(double u, double v) {
			return true;
		}
	};

	public abstract boolean isValidIntersectionPoint(double u, double v);
}
