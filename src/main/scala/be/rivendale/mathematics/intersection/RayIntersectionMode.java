package be.rivendale.mathematics.intersection;

import be.rivendale.mathematics.MathematicalUtilities;

/**
 * Determines the intersection mode of the {@link be.rivendale.mathematics.intersection.Intersection intersection algorithm} regarding to the involved
 * {@link be.rivendale.mathematics.Ray}.
 * <p>When an intersection with the ray occurs, the actual position of the intersection relative to the two points
 * that define the ray (the {@link be.rivendale.mathematics.Ray#getOrigin()} , and the {@link be.rivendale.mathematics.Ray#getPassThroughPoint()}) determines to treat
 * the intersection as valid or not depending on the situation. These enumeration constants encapsulate these
 * situations.</p>
 */
public enum RayIntersectionMode {
    anywhere {
        @Override
        boolean isValidIntersectionPoint(double t) {
            return true;
        }
    },

	/**
	 * When an intersection occurs on the ray, it is only regarded valid when the point of intersection lies after the
	 * pass-through point (starting from the origin).
	 */
    afterPassThroughPoint {
        boolean isValidIntersectionPoint(double t) {
            return t > 1;
        }
    },

	/**
	 * When an intersection occurs on the ray, it is only regarded valid when the point of intersection lies between
	 * the origin and the pass-through point. This way you actually get the behaviour of a line segment.
	 */
    betweenOriginAndPassThroughPoint {
        boolean isValidIntersectionPoint(double t) {
            return MathematicalUtilities.between(0, 1, t);
        }
    };

    abstract boolean isValidIntersectionPoint(double t);
}
