package be.rivendale.mathematics;

/**
 * @deprecated To be replaced by VectorNovo
 */
@Deprecated
public interface Point {
    double getX();

    double getY();

    double getZ();

    /**
     * Adds two points from eachother.
     * This is done by adding each coordinate from the other like this:
     * <code>x' = x1 + x2</code>, <code>y' = y1 + y2</code>, <code>z' = z1 + z2</code>
     * The state of the two points involved is left unchanged.
     * @param point The point to add from this one.
     * @return The result of the addition, which is a new point.
     */
    Point add(Point point);
    Point $plus(Point point);

    /**
     * Subtracts two points from eachother.
     * This is done by subtracting each coordinate from the other like this:
     * <code>x' = x1 - x2</code>, <code>y' = y1 - y2</code>, <code>z' = z1 - z2</code>
     * The state of the two points involved is left unchanged.
     * @param point The point to subtract from this one.
     * @return The result of the subtraction, which is a new point.
     */
    Point subtract(Point point);
    Point $minus(Point point);

    /**
     * Divides all coordinates of this point by the scalar specified
     * and returns the results in a new point.
     * The state of 'this' point is left unchanged.
     * @param scalarValue The value by which to divide each point coordinate.
     * @return The result of the division, as a new point.
     */
    Point divide(double scalarValue);
    Point $div(double scalarValue);

    Point $times(double scalarValue);

    /**
     * Returns this point as a vector instance.
     * This is the correct way to convert a point to a vector.
     * @return A vector, that has the same x, y and z values as the point.
     */
    Vector asVector();
}
