package be.rivendale.ghetto;

import be.rivendale.material.Color;
import be.rivendale.mathematics.Point;
import be.rivendale.mathematics.Triple;
import be.rivendale.mathematics.Vector;
import be.rivendale.mathematics.Vertex;

public class Shader {
    public static Color shade(Vertex light, Vector normal, Color color, Point position) {
        Vector lightVector = Triple.vectorBetweenPoints(position, light).invert();
        double angle = lightVector.angle(normal);
        double lightIntensity = angle / Math.PI;
//        if(angle > Math.PI / 2.0) {
//            return Color.GREEN;
//        }
        return light.getColor().multiply(lightIntensity);
    }

    private static Color mergeColors(Color a, Color b) {
        return new Color(
                (a.getRed() + b.getRed()) / 2,
                (a.getGreen() + b.getGreen()) / 2,
                (a.getBlue() + b.getBlue()) / 2
        );
    }
}
