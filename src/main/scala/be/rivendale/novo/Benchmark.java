package be.rivendale.novo;

import be.rivendale.mathematics.*;

import java.util.concurrent.Callable;

public class Benchmark {
    // Plane
    private static final Point pa = new Triple(-8f, -4.5f, 0f);
    private static final Point pb = new Triple(8f, -4.5f, 0f);
    private static final Point pc = new Triple(8f, 4.5f, 0f);
    private static final Plane plane = new Plane(pa, pb, pc);

    // Ray
    private static final Point ra = new Triple(0, 0, -5);
    private static final Point rb = new Triple(6, 5, 10);
    private static final Ray ray = new Ray(ra, rb);

    private static Callable<Integer> kevinPlane = new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            final Vector a = ray.direction();
            final Vector b = plane.getB().subtract(plane.getA()).asVector();
            final Vector c = plane.getC().subtract(plane.getA()).asVector();

            final Vector pr = ray.getOrigin().subtract(plane.getA()).asVector();

            final double m = a.dotProduct(c.crossProduct(b));

            final double u = a.dotProduct(c.crossProduct(pr)) / m;
            final double v = a.dotProduct(pr.crossProduct(b)) / m;

            Point point = plane.pointOnPlane(u, v);

            return (int)((point.getX() + point.getY() + point.getZ()) * 1000);
        }
    };
    private static Callable<Integer> stack = new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            //        final Vector a = ray.direction();
            final double ax = ray.getPassThroughPoint().getX() - ray.getOrigin().getX();
            final double ay = ray.getPassThroughPoint().getY() - ray.getOrigin().getY();
            final double az = ray.getPassThroughPoint().getZ() - ray.getOrigin().getZ();

//        final Vector b = plane.getB().subtract(plane.getA()).asVector();
            final double bx = plane.getB().getX() - plane.getA().getX();
            final double by = plane.getB().getY() - plane.getA().getY();
            final double bz = plane.getB().getZ() - plane.getA().getZ();

//        final Vector c = plane.getC().subtract(plane.getA()).asVector();
            final double cx = plane.getC().getX() - plane.getA().getX();
            final double cy = plane.getC().getY() - plane.getA().getY();
            final double cz = plane.getC().getZ() - plane.getA().getZ();

//        final Vector pr = ray.getOrigin().subtract(plane.getA()).asVector();
            final double prx = ray.getOrigin().getX() - plane.getA().getX();
            final double pry = ray.getOrigin().getY() - plane.getA().getY();
            final double prz = ray.getOrigin().getZ() - plane.getA().getZ();

//        final Vector bcrossc = b.crossProduct(c);
            final double bcrosscx = by * cz - bz * cy;
            final double bcrosscy = bz * cx - bx * cz;
            final double bcrosscz = bx * cy - by * cx;

//        final double t = pr.dotProduct(bcrossc) / a.invert().dotProduct(bcrossc);
            final double nom = prx * bcrosscx + pry * bcrosscy + prz * bcrosscz;
            final double denom = -ax * bcrosscx + -ay * bcrosscy + -az * bcrosscz;
            final double t = nom / denom;

//        return ray.pointOnRay(t);
//        p = p0 + v*t
            final double x = ray.getOrigin().getX() + ax * t;
            final double y = ray.getOrigin().getY() + ay * t;
            final double z = ray.getOrigin().getZ() + az * t;

            return (int)((x + y + z) * 1000);
        }
    };
    private static Callable<Integer> original = new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            final Vector c = ray.direction();
            final Vector oa = plane.getA().subtract(ray.getOrigin()).asVector();

            final Vector planeNormal = plane.normal();

            final double t = oa.dotProduct(planeNormal) / c.dotProduct(planeNormal);

//        System.out.println(t);
            Point point = ray.pointOnRay(t);
            return (int)((point.getX() + point.getY() + point.getZ()) * 1000);
        }
    };
    private static Callable<Integer> kevinRay = new Callable<Integer>() {
        @Override
        public Integer call() throws Exception {
            final Vector a = ray.direction();
            final Vector b = plane.getB().subtract(plane.getA()).asVector();
            final Vector c = plane.getC().subtract(plane.getA()).asVector();

            final Vector pr = ray.getOrigin().subtract(plane.getA()).asVector();
            final Vector bcrossc = b.crossProduct(c);

            final double t = pr.dotProduct(bcrossc) / a.invert().dotProduct(bcrossc);
            Point point = ray.pointOnRay(t);
            return (int)((point.getX() + point.getY() + point.getZ()) * 1000);
        }
    };

    public static void main(String[] args) {
        long iterations = 100_000_000L;
        int expectedValue = 3666;

        benchmark("Original", original, iterations, expectedValue);
        benchmark("Stack", stack, iterations, expectedValue);
        benchmark("KevinRay", kevinRay, iterations, expectedValue);
        benchmark("KevinPlane", kevinPlane, iterations, expectedValue);
    }

    public static <T> void benchmark(String name, Callable<T> task, long iterations, T expectedResult) {
        System.out.format("Starting benchmark '%s'\n", name);

        System.out.println("\t - Warming up");
        benchmarkRun(task, iterations, expectedResult);

        double total = 0;
        final int runs = 10 ;
        for(int i = 1; i <= runs; i++) {
            double result = benchmarkRun(task, iterations, expectedResult);
            total += result;
            System.out.format("\t - Results of run %d: %.3fms\n", i, result);
        }
        System.out.format("\t - Average: %.3fms\n", total / runs);
    }

    private static <T> double benchmarkRun(Callable<T> task, long iterations, T expectedResult) {
        System.gc();
        double start = System.nanoTime();
        for(long i = 0; i < iterations; i++) {
            try {
                T actualResult = task.call();
                if(!actualResult.equals(expectedResult)) {
                    throw new RuntimeException("Assertion failed. Expected '" + expectedResult + "' but was '" + actualResult + "'");
                }
            } catch (Exception exception) {
                throw new RuntimeException("Benchmark failed", exception);
            }
        }
        double stop = System.nanoTime();
        return (stop - start) / (1000.0 * 1000.0);
    }

}
