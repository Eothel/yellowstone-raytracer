Todo:
-----

1. Change all Validate.xxx() to assertions
    (they sometimes have unexpected but huge impact)

2. Convert math library to Scala
    (offers LOTS of readability improvements, due to operator overloading)

3. Simplify Ray - Line - LineSegment as just Line (in scala)

4. Simplify Vector and Point to just Vector
    - for now you could just treat everything as Triple, and refactor out the interfaces later
    (simpler Point to Vector transition in algorithms - no need to cast all the time)

5. Reimplement möller-trubmore the simple way
    (i've been going overboard on the reusability before - causing too generic and slow code)
    (also incorporates backface culling in the intersection test itself)

6. Implement intersection using lambda
    Need some way of telling it what it needs as output
    (barycentric coordinates, t value, ...)